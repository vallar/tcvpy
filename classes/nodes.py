"""
This file contains several classes to better handle the reading from TCV nodes

Classes
node_core: contains some function shared in the following two
node_cdf: reads and plots a signal read from a cdf file
node_mds: reads, plots and deploys a signal from mds tree and to cdf file
    Example:
        node = nodes.node_mds(shot, signame)

File: nodes.py
"""
import MDSplus as mds
import xarray as xr
from numpy import *
import collections

class node_core():
    """
    Class defining a node
    
    Attributes:
        shot: int </br>
            Shot number
        signame: str </br>
            String with the node to read
    Example:
        test = n.node(shot, signame)
    """
    def __init__(self, shot, signame, quiet=False, transpose=False):
        """
        Building the class
        """
        self.shot = shot
        self.signame = signame
        
    def store_to_netcdf(self, filename):
        """
        Wrapper to store the signal to a netcdf file

        Attrs:
            filename: str <br>
                Name of file where to store the data

        """
        self.signal.to_netcdf(filename, engine='scipy')

            
    def _plot(self, swap=False):
        """
        Wrapper for self.signal.plot()

        Args:
            swap: bool </br>
                if true, will make a plot with x=rho_tcv, y=time
        """
        if not swap:
            self.signal.plot()
        else:
            self.signal.T.plot()
        
class node_cdf(node_core):
    """
    """
    def __init__(self, fname=None):
        self.fname=fname
        self._read_from_netcdf()
        shot=self.signal.shot
        signame=self.signal.MDS_node
        node_core.__init__(self, shot, signame)

    def _read_from_netcdf(self):
        """
        Wrapper to read the signal from a netcdf file

        """
        self.signal = xr.open_dataarray(self.fname, engine='scipy')        


class node_mds(node_core):
    """
    """
    def __init__(self, shot, signame, quiet=False, transpose=False, dim_labels=None):
        node_core.__init__(self, shot, signame)
        self.transpose=transpose
        self.quiet = quiet
        self.dim_labels = dim_labels
        self.tree = self._connect_to_server(self.shot)
        self.signal = self._readsignal(self.tree, self.signame, self.quiet, self.transpose)
        if not hasattr(self, 'label'):
            self.label=self.signame
        self._assign_dimensions()
        self.signal=self.signal.assign_attrs({'shot':shot, 'units': self.label})

    def _assign_dimensions(self):
        """
        From a connection you cannot retrieve the dimensions, so I am doing it manually here
        """
        if self.dim_labels is not None:
            tmp=zip(self.signal.dims, self.dim_labels)
            dim_dict = dict(tmp)
            self.signal = self.signal.rename(dim_dict)
                
    @classmethod
    def _connect_to_server(cls, shot):
        """ connects to MDS server
        This script opens a connection to lac.epfl.ch
    
        Args:
        shot : int <br>
            Shot number

        Returns:
        tree (obj): 
            returns the tree object where to read data from	
        """
        conn = mds.Connection('tcvdata.epfl.ch')
        conn.openTree('tcv_shot', shot)
        return conn
    
    @classmethod
    def _readsignal(cls, conn, signame, quiet, transpose):
        """ reads signal
        Interface to read the signal from a connection

        Args:
        conn: mds.connection <br>
            connection object obtained with _connect_to_server
        signame: str <br>
            String with the name of the node you want
        Returns:
        signal (arr): the data you want
        """
        if not quiet:
            print(f'Reading signal {signame}')    
        signal_dims = collections.OrderedDict({})
        signal_attrs = {'MDS_node': signame}
        _signal = conn.get(signame)
        for i in range(size(shape(_signal.data()))):
            #label = conn.get(f'dim_of({signame},{i})').getUnits()
            data = conn.get(f'dim_of({signame},{i})').data()
            signal_dims[str(i)] = data

        data=_signal.data()    
        if transpose:
            data=data.T
        signal = xr.DataArray(data, dims=signal_dims.keys(), coords=signal_dims.values(), attrs=signal_attrs)
        return signal
