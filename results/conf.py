"""@package conf
This class defines the names and the method for the results:conf nodes

Classes
conf
conf_1d
conf_2d

ne
te

File: conf.py
"""
from tcvpy.classes.nodes import node_mds as node

#### SUPERCLASS CONF
class conf(node):
    """
    Class defining the conf node database for reading and plotting
    
    Args:
        shot : int <br>
            Shot number
        signame: str <br>
            String with the name of the node you want
        trial_indx: int <br>
            Desired trial_indx. Same convention as MDS nodes (0=test, 1=LIUQE1, ecc) 
        transpose: bool <br>
            if true, store the array as transposed. Needed for some 2D data
        
    Notes:
        tcv_shot::top.results.conf:*
    """
    def __init__(self, shot, signame, trial_indx, transpose, dim_labels):
        node.__init__(self, shot, signame, transpose=transpose, dim_labels=dim_labels)
        self._apply_trial_indx(trial_indx)
        self._getcomments()
        
    def _get_trial_indx(self, trial_indx):
        """
        Returns the trialindx desired, if any. Otherwise the ok_trialindx

        Args:
            trial_indx: int <br>
                Desired trial_indx. Same convention as MDS nodes (0=test, 1=LIUQE1, ecc)        
        """
        self._get_oktrialindx()

        if trial_indx==0:
            self.trial_indx = self.ok_trial_indx
        else:
            self.trial_indx = trial_indx
        
    def _get_oktrialindx(self):
        """
        Reading which is the ok_trialindx

        Notes
            \tcv_shot::top.results.conf:ok_trialindx
        """
        ok_trial_indx_signal = node._readsignal(self.tree, r'\tcv_shot::top.results.conf:ok_trialindx', quiet=True, transpose=True)
        self.ok_trial_indx = ok_trial_indx_signal.mean(dtype=int)

    def _apply_trial_indx(self, trial_indx):
        """
        Filtering the wanted signal using the trial_indx

        Args:
            trial_indx: int </br>
                Desired trial_indx. Same convention as MDS nodes (0=test, 1=LIUQE1, ecc)
        """
        if trial_indx is not None:
            self._get_trial_indx(trial_indx)
            self._signal_alltrial_indx = self.signal.copy()
            if self.dims==2:
                self.signal = self.signal.loc[:,:,self.trial_indx]
            elif self.dims==1:
                self.signal = self.signal.loc[:,self.trial_indx]
            attrs={'ok_trial_indx':self.ok_trial_indx.data, 'trial_indx':self.trial_indx}
        else:
            attrs={'ok_trial_indx':'None', 'trial_indx':'No trial_indx for this node'}
        self.signal = self.signal.assign_attrs(attrs)

    def _getcomments(self):
        """
        Reading the comment to conf nodes

        Notes:
            \tcv_shot::top.results.conf:comments
        """
        self.comments_signal = self.tree.get(r'\tcv_shot::top.results.conf:comments:trial')
        self.signal = self.signal.assign_attrs({'comments':self.comments_signal.data()[self.trial_indx,0]})

#### SUPERCLASS CONF 1D
class conf_1d(conf):
    """
    Class for 1-dimensional signals in conf nodes

    Args:
        shot : int <br>
            Shot number
        signame: str <br>
            String with the name of the node you want
        trial_indx: int <br>
            Desired trial_indx. Same convention as MDS nodes (0=test, 1=LIUQE1, ecc) 
    """
    def __init__(self, shot, signame, trial_indx):
        self.dims=1
        dim_labels = ['t [s]', 'trial_indx']
        conf.__init__(self, shot, signame, trial_indx, transpose=True, dim_labels=dim_labels)
        self.data = self.signal.data
        self.t = self.signal.coords['t [s]']
    def plot(self):
        """
        Wrapper for self._plot()
        """
        self._plot(swap=False)


#### SUPERCLASS CONF 2D
class conf_2d(conf):
    """
    Class for 2-dimensional signals in conf nodes
    
    Args:
        shot : int <br>
            Shot number
        signame: str <br>
            String with the name of the node you want
        trial_indx: int <br>
            Desired trial_indx. Same convention as MDS nodes (0=test, 1=LIUQE1, ecc) 
        transpose: bool <br>
            if true, store the array as transposed. Needed for some 2D data
    """
    
    def __init__(self, shot, signame, trial_indx):
        self.dims=2
        dim_labels = [r'$\rho$ TCV', 't [s]', 'trial_indx']
        conf.__init__(self, shot, signame, trial_indx, transpose=True, dim_labels=dim_labels)
        self.t = self.signal.coords['t [s]']
        self.rho = self.signal.coords[r'$\rho$ TCV']
        self.data = self.signal.data
    def plot(self, swap=True):
        """
        Wrapper for self._plot()

        Attributes:
            swap: bool </br>
                if true, will make a plot with x=rho_tcv, y=time
        """
        self._plot(swap)

###############################################################################
#CLASSES 1D
class ptot_ohm(conf_1d):
    """
    Class for ptot_ohm signal in conf nodes

    Notes:
        '\tcv_shot::top.results.conf:ptot_ohm:trial'
    """
    def __init__(self, shot):
        signame=r'\tcv_shot::top.results.conf:ptot_ohm'
        self.label = 'ptot_ohm [W]'
        trial_indx=None
        conf_1d.__init__(self, shot, signame, trial_indx)

class zeff(conf_1d):
    """
    Class for ptot_ohm signal in conf nodes

    Notes:
        '\tcv_shot::top.results.conf:z_eff:trial'
    """
    def __init__(self, shot, trial_indx):
        signame=r'\tcv_shot::top.results.conf:z_eff:trial'
        self.label = 'z_eff'
        conf_1d.__init__(self, shot, signame, trial_indx)

###############################################################################
#CLASSES 2D        
class ne(conf_2d):
    """
    Class for ne signal in conf nodes

    Notes:
        '\tcv_shot::top.results.conf:ne:trial'
    """
    def __init__(self, shot, trial_indx=0):
        signame = r'\tcv_shot::top.results.conf:ne:trial'
        self.label = 'n_e [m^-3]'
        conf_2d.__init__(self, shot, signame, trial_indx)
        
class te(conf_2d):
    """
    Class for te signal in conf nodes

    Notes:
        '\tcv_shot::top.results.conf:ne:trial'
    """
    def __init__(self, shot, trial_indx=0):
        signame = r'\tcv_shot::top.results.conf:te:trial'
        self.label = 'T_e [eV]'
        conf_2d.__init__(self, shot, signame, trial_indx)

class ti(conf_2d):
    """
    Class for ti signal in conf nodes

    Notes:
        '\tcv_shot::top.results.conf:ti:trial'
    """
    def __init__(self, shot, trial_indx=0):
        signame = r'\tcv_shot::top.results.conf:ti:trial'
        self.label = 'T_i [eV]'
        conf_2d.__init__(self, shot, signame, trial_indx)
