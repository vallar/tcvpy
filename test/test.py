#!/usr/bin/env python3
import sys
sys.path.append('/home/vallar/pythonscripts/')
import tcvpy.results.conf as conf
import tcvpy.classes.nodes as nodes
import matplotlib.pyplot as plt

shot=65052;
trial_indx=1;
fname='/tmp/test.nc'
print(f'Testing reading of shot {shot}, trial_indx {trial_indx}, ne')
ne=conf.ne(shot, trial_indx)
ne.plot()
ne.store_to_netcdf(fname)
print(f'Correctly read and write from {fname}')
ss=nodes.node_cdf(fname)
plt.figure()
ss._plot(swap=True)
plt.show()
